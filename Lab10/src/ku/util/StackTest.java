package ku.util;


import static org.junit.Assert.*;

import java.util.EmptyStackException;

import junit.framework.Assert;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author 5710546186
 *
 */
public class StackTest {
	
	private Stack stack;
	
	@Before
	public void setUp(){
		StackFactory.setStackType(0);
		stack = StackFactory.makeStack(5);
	}

	@Test
	public void testCapacity() {
		Assert.assertEquals(5,stack.capacity());
	}
	
	@Test
	public void testIsEmpty() {
		assertTrue( stack.isEmpty() );
		assertFalse( stack.isFull() );
		assertEquals( 0, stack.size() );
	}
	
	@Test
	public void testIsFull() {
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		assertTrue( stack.isFull() );
		assertFalse( stack.isEmpty() );
		assertEquals( 5, stack.size() );
	}
	
	@Test
	public void testPeek() {
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		assertEquals( 5, stack.peek() );
		
	}
	
	@Test
	public void testEmptyPeek() {
		assertEquals( null, stack.peek() );
	}

	@Test( expected=EmptyStackException.class )
	public void testPopEmpty() {
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		assertEquals( 5, stack.pop() );
		assertEquals( 4, stack.pop() );
		assertEquals( 3, stack.pop() );
		assertEquals( 2, stack.pop() );
		assertEquals( 1, stack.pop() );
		stack.pop();
	}
	
//	@Test
//	public void testOffByOne() {
//		stack.push(1);
//		stack.push(2);
//		stack.push(3);
//		stack.pop();
//		stack.pop();
//		stack.peek();
//		stack.push(2);
//		assertEquals( 2,stack.size());
//	}
	
	@Test( expected=IllegalArgumentException.class )
	public void testPush() {
		stack.push(null);
	}
	
	@Test( expected=IllegalStateException.class )
	public void testInvalidPush() {
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		stack.push(6);
	}
	
	@Test
	public void testSize() {
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		assertEquals( 5, stack.size() );
	}
}
